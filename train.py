"""

Pytorch implementation of Pointer Network.

http://arxiv.org/pdf/1506.03134v1.pdf.

"""

import torch
import torch.optim as optim
import torch.backends.cudnn as cudnn
from torch.autograd import Variable
from torch.utils.data import DataLoader

import numpy as np
import argparse
from tqdm import tqdm
import matplotlib.pyplot as plt
from meunet import MEUNET,TBUNET
from datagen import corefdataset

parser = argparse.ArgumentParser(description="Pytorch implementation of Pointer-Net")

# Data
parser.add_argument('--glove_file',default='/home/aniket/e2e-coref/glove.840B.300d.txt',help='glove file loc')
parser.add_argument('--data_file',default='/home/aniket/neuralcoref/neuralcoref/final_data.pickle',help='data file loc')
parser.add_argument('--train_size', default=1000000, type=int, help='Training data size')
parser.add_argument('--val_size', default=10000, type=int, help='Validation data size')
parser.add_argument('--test_size', default=10000, type=int, help='Test data size')
parser.add_argument('--batch_size', default=2, type=int, help='Batch size')
# Train
parser.add_argument('--nof_epoch', default=512, type=int, help='Number of epochs')
parser.add_argument('--lr', type=float, default=2.5e-4, help='Learning rate')
# GPU
parser.add_argument('--gpu', default=True, action='store_true', help='Enable gpu')

# LSTM Network
parser.add_argument('--embedding_size', type=int, default=512, help='Embedding size')
parser.add_argument('--hiddens', type=int, default=16, help='Number of hidden units')
parser.add_argument('--nof_lstms', type=int, default=2, help='Number of LSTM layers')
parser.add_argument('--bidir', default=True, action='store_true', help='Bidirectional')

#Transformer network

parser.add_argument('--dropout', type=float, default=0.1,
                            help='dropout probability')
parser.add_argument('--attention-dropout', type=float, default=0.1,
                    help='dropout probability for attention weights')
parser.add_argument('--relu-dropout', type=float, default=0.1,
                    help='dropout probability after ReLU in FFN')
parser.add_argument('--encoder-embed-path', type=str, default='/home/aniket/e2e-coref/glove.840B.300d.txt',
                    help='path to pre-trained encoder embedding')
parser.add_argument('--encoder-embed-dim', type=int, default=512,
                    help='encoder embedding dimension')
parser.add_argument('--encoder-ffn-embed-dim', type=int, default=512,
                    help='encoder embedding dimension for FFN')
parser.add_argument('--encoder-layers', type=int, default=6,
                    help='num encoder layers')
parser.add_argument('--encoder-attention-heads', type=int, default=8,
                    help='num encoder attention heads')
parser.add_argument('--encoder-normalize-before', default=True,
                    help='apply layernorm before each encoder block')
parser.add_argument('--encoder-learned-pos', default=False,
                    help='use learned positional embeddings in the encoder')
parser.add_argument('--max-source-positions', type=int, default=1024,
                            help='dropout probability')
parser.add_argument('--no-token-positional-embeddings', default=False, action='store_true',
                            help='if set, disables positional embeddings (outside self attention)')

params = parser.parse_args()

if params.gpu and torch.cuda.is_available():
    USE_CUDA = True
    print('Using GPU, %i devices.' % torch.cuda.device_count())
else:
    USE_CUDA = False



dataset = corefdataset(params.glove_file,params.data_file,params.embedding_size,params.batch_size,params.encoder_embed_dim)

#model = MEUNET(dataset, params.embedding_size,
#                   params.hiddens,
#                   params.nof_lstms,
#                   params.dropout,
#                   params.bidir)
model=TBUNET(dataset,params)

if USE_CUDA:
    model.cuda()
    #net = torch.nn.DataParallel(model, device_ids=range(torch.cuda.device_count()))
    #cudnn.benchmark = True
weights=torch.tensor([1e-2,1]).cuda().float()
CCE = torch.nn.CrossEntropyLoss(weight=weights,size_average=True)
model_optim = optim.Adam(filter(lambda p: p.requires_grad,
                                model.parameters()),
                         lr=params.lr)
#model.load_state_dict(torch.load('best_model.pt')['net'].state_dict())
losses = []


for epoch in range(params.nof_epoch):
    batch_loss =0 
    #iterator = tqdm(dataloader, unit='Batch')
    iou=0
    acc=0
    gradient_norm=0
    true=[]
    preds=[]
    for i in tqdm(range(dataset.numbatches)):
        sample_batched=dataset.getitem(i)
        #iterator.set_description('Batch %i/%i' % (epoch+1, params.nof_epoch))

        train_batch = Variable(sample_batched['Points'])
        target_batch = Variable(sample_batched['Solution'])
        train_seqlens=Variable(sample_batched['Seqlen'])
        if USE_CUDA:
            train_batch = train_batch.cuda()
            target_batch = target_batch.cuda()
        o = model(train_batch,train_seqlens)
        
        loss = CCE(o, target_batch)


        batch_loss+=loss.data

        model_optim.zero_grad()
        loss.backward()
        batch_loss+=loss.data/dataset.numbatches
        norm = torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
        model_optim.step()
        target_batch_numpy=target_batch.cpu().numpy()
        o=o.data.cpu().numpy()
        o=np.argmax(o,axis=1)
        equal=np.sum(target_batch_numpy==o)
        total=o.shape[0]*o.shape[2]*o.shape[1]
        acc+=float(equal)/float(total)
        true.append(target_batch_numpy[-1,:,:])
        preds.append(o[-1,:,:])
        gradient_norm+=norm

        state = {
            'net': model
            }
    with open('best_model.pt', 'wb') as f:
        torch.save(state, f) 
    testacc=0   
    for i in tqdm(range(dataset.testnumbatches)):
        sample_batched=dataset.gettestitem(i)
        #iterator.set_description('Batch %i/%i' % (epoch+1, params.nof_epoch))

        train_batch = Variable(sample_batched['Points'])
        target_batch = Variable(sample_batched['Solution'])
        if USE_CUDA:
            train_batch = train_batch.cuda()
            target_batch = target_batch.cuda()
        
        o = model(train_batch)
        target_batch_numpy=target_batch.cpu().numpy()
        o=o.data.cpu().numpy()
        o=np.argmax(o,axis=1)
        equal=np.sum(target_batch_numpy==o)
        total=o.shape[0]*o.shape[2]*o.shape[1]
        testacc+=float(equal)/float(total)  

    print(str(epoch)+' loss:'+str(batch_loss)+' gradient_norm:'+str(gradient_norm)+' trainaccuracy:'+str(acc/dataset.numbatches)+' testacc:'+str(testacc/dataset.testnumbatches))
f, axarr = plt.subplots(2,9)
for i in range(len(preds)):
    axarr[0,i].imshow(preds[i])
    axarr[1,i].imshow(true[i])
plt.show()
