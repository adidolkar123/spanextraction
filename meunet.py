import torch
import torch.nn as nn
from torch.nn import Parameter
import torch.nn.functional as F
from unet import UNet
from fairseq.models.transformer import TransformerEncoder 
class MEUNET(nn.Module):
	def __init__(self,dataset,embed_size,hidden,num_layers,dropout,bidir):
		super(MEUNET, self).__init__()
		self.embedding_dim = embed_size
		self.embedding = nn.Embedding(dataset.embed_matrix.shape[0], self.embedding_dim)
		embed_matrix_tensor=torch.from_numpy(dataset.embed_matrix).cuda()
		self.embedding.load_state_dict({'weight':embed_matrix_tensor})
		self.lstm=nn.LSTM(embed_size,hidden,num_layers,dropout=dropout,bidirectional=bidir)
		self.unet=UNet(2,in_channels=1,depth=3)

	def forward(self,x):
		x=x.transpose(1,0)
		x=self.embedding(x)
		x,(h,c)=self.lstm(x)
		x=x.transpose(1,0)
		x=x.transpose(2,1)
		x=x.unsqueeze(1)
		x=self.unet(x)
		return x

class TBUNET(nn.Module):
	def __init__(self,dataset,params):
		super(TBUNET, self).__init__()
		self.embedding_dim = params.encoder_embed_dim
		self.embedding = nn.Embedding(dataset.embed_matrix.shape[0], self.embedding_dim,padding_idx=dataset.fairseq_dictionary.pad())
		embed_matrix_tensor=torch.from_numpy(dataset.embed_matrix).cuda()
		self.embedding.load_state_dict({'weight':embed_matrix_tensor})
		self.TransformerEncoder=TransformerEncoder(params,dataset.fairseq_dictionary,self.embedding,left_pad=False)
		self.unet=UNet(2,in_channels=1)

	def forward(self,x,seqlens):
		x=self.TransformerEncoder(x,seqlens)
		x=x['encoder_out']
		x=x.transpose(1,0)
		x=x.transpose(2,1)

		x=x.unsqueeze(1)
		x=self.unet(x)
		return x