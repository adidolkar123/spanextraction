from gensim.test.utils import common_texts, get_tmpfile
from gensim.models import Word2Vec
import pickle
from random import shuffle
path = get_tmpfile("word2vec.model")

with open('/home/aniket/neuralcoref/neuralcoref/final_data.pickle','rb') as f:
    data_list=pickle.load(f,encoding='utf-8')
    sentences_list=[]
    for d in data_list:
        sents=d[2]
        for s in sents:
            sentences_list.append(s.split())

    shuffle(sentences_list)
    model = Word2Vec(sentences_list, size=512, window=5, min_count=1, workers=4)
    model.train(sentences_list, total_examples=len(sentences_list), epochs=200)
    model.save('word2vec.model')