import numpy as np
import itertools
from torch.utils.data import Dataset
from tqdm import tqdm
import pickle
import torch
from random import shuffle
from gensim.models import Word2Vec
from fairseq.data.dictionary import Dictionary
class corefdataset(Dataset):
    def __init__(self,glove_file,data_file,emb_dim,batch_size,hidden_size):
        self.glove_file=glove_file
        self.hidden_size=hidden_size
        self.vocab={}
        self.id2word={}
        self.id2vec={}
        self.fairseq_dictionary=Dictionary()
        with open(data_file,'rb') as f:
            self.data_list=pickle.load(f,encoding='utf-8')
            new_data_list=[]
            for d in self.data_list:
                if len(d[2])>100:
                    continue
                else:
                    new_data_list.append(d)
            self.data_list=new_data_list
            
            model = Word2Vec.load("word2vec.model")
            print('parsing data')
            for d in tqdm(self.data_list):
                doc=d[0]
                doc=doc.split()
                for k in doc:
                    if k not in self.vocab:
                        self.fairseq_dictionary.add_symbol(k)
                        self.vocab[k]=len(self.vocab)
                        self.id2word[self.vocab[k]]=k
                        self.id2vec[self.vocab[k]]=model.wv[k]
                        
        self.embed_matrix=np.zeros((len(self.vocab),emb_dim))
        for i in self.vocab:

            self.embed_matrix[self.vocab[i],:]=model.wv[i]
        new_data_list2=[]
        for d in self.data_list:
            include=True
            for k in d[2]:
                for w in k.split():
                    if w not in self.vocab:
                        include=False
            if include==True:
                new_data_list2.append(d)
        self.data_list=new_data_list2
        self.data_list,self.test_list=self.data_list[:2000],self.data_list[2000:]
        self.sentences_list=[]
        self.sentences_list_batches=[]
        for d in self.data_list:
            sents=d[2]
            corefs=d[1]
            for s,c in zip(sents,corefs):
                if len(s.split())<10:
                    continue
                self.sentences_list.append([s,c])

        #shuffle(self.sentences_list)
        print(len(self.sentences_list))
        for i in range(0,len(self.sentences_list),batch_size):
            self.sentences_list_batches.append(self.sentences_list[i:i+batch_size])

        self.numbatches=len(self.sentences_list_batches)
        shuffle(self.sentences_list_batches)
        self.testnumbatches=len(self.test_list)
        
    def __len__(self):
        return self.numbatches
    def getitem(self,idx):
        cur=self.sentences_list_batches[idx]
        maxseqlensent=0
        for c in cur:
            if len(c[0].split())>maxseqlensent :
                maxseqlensent=len(c[0].split())
        nearestpowerof2=1
        while nearestpowerof2<=maxseqlensent:
            nearestpowerof2=nearestpowerof2*2
        
        points=np.zeros((len(cur),nearestpowerof2))
        solutions=np.zeros((len(cur),self.hidden_size,nearestpowerof2))
        seqlens=np.zeros((len(cur)))
        

        for i,c in enumerate(cur):
            d1=c[0].split()
            
            seqlens[i]=len(d1)
            for j,d in enumerate(d1):
                points[i,j]=self.vocab[d]
        
        for i,c1 in enumerate(cur):
            for j,c in enumerate(c1[1]):
                for k in range(c['start'],c['end']+1):
                    solutions[i,:,k]=1         
        points=torch.from_numpy(points).long()
        solutions=torch.from_numpy(solutions).long()
        seqlens=torch.from_numpy(seqlens).long()
        sample = {'Points':points, 'Solution':solutions,'Seqlen':seqlens}

        return sample
    def gettestitem(self,idx):
        cur=self.test_list[idx]
        sents=cur[2]

        corefs=cur[1]
        maxseqlensent=0
        for s in sents:
            if len(s.split())>maxseqlensent :
                maxseqlensent=len(s.split())
        nearestpowerof2=1
        while nearestpowerof2<maxseqlensent:
            nearestpowerof2=nearestpowerof2*2
        points=np.zeros((len(sents),nearestpowerof2))
        solutions=np.zeros((len(sents),2*self.hidden_size,nearestpowerof2))
        
        for i,c in enumerate(sents):
            d1=c.split()
            for j,d in enumerate(d1):
                points[i,j]=self.vocab[d]

        for i,c1 in enumerate(corefs):
            for j,c in enumerate(c1):
                for k in range(c['start'],c['end']+1):
                    solutions[i,:,k]=1
                    

                    

        points=torch.from_numpy(points).long()
        solutions=torch.from_numpy(solutions).long()

        sample = {'Points':points, 'Solution':solutions}

        return sample


if __name__=='__main__':
    c=corefdataset('/home/aniket/e2e-coref/glove.840B.300d.txt','/home/aniket/neuralcoref/neuralcoref/final_data.pickle',300,2)