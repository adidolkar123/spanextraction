import torch
import torch.optim as optim
import torch.backends.cudnn as cudnn
from torch.autograd import Variable
from torch.utils.data import DataLoader

import numpy as np
import argparse
from tqdm import tqdm
import matplotlib.pyplot as plt
from meunet import MEUNET
from datagen import corefdataset
parser = argparse.ArgumentParser(description="Pytorch implementation of Pointer-Net")


# Data
parser.add_argument('--glove_file',default='/home/aniket/e2e-coref/glove.840B.300d.txt',help='glove file loc')
parser.add_argument('--data_file',default='/home/aniket/neuralcoref/neuralcoref/final_data.pickle',help='data file loc')
parser.add_argument('--train_size', default=1000000, type=int, help='Training data size')
parser.add_argument('--val_size', default=10000, type=int, help='Validation data size')
parser.add_argument('--test_size', default=10000, type=int, help='Test data size')
parser.add_argument('--batch_size', default=3, type=int, help='Batch size')
# Train
parser.add_argument('--nof_epoch', default=300, type=int, help='Number of epochs')
parser.add_argument('--lr', type=float, default=2.5e-4, help='Learning rate')
# GPU
parser.add_argument('--gpu', default=True, action='store_true', help='Enable gpu')
# TSP
parser.add_argument('--nof_points', type=int, default=5, help='Number of points in TSP')
# Network
parser.add_argument('--embedding_size', type=int, default=300, help='Embedding size')
parser.add_argument('--hiddens', type=int, default=32, help='Number of hidden units')
parser.add_argument('--nof_lstms', type=int, default=2, help='Number of LSTM layers')
parser.add_argument('--dropout', type=float, default=0.5, help='Dropout value')
parser.add_argument('--bidir', default=True, action='store_true', help='Bidirectional')
params = parser.parse_args()

if params.gpu and torch.cuda.is_available():
    USE_CUDA = True
    print('Using GPU, %i devices.' % torch.cuda.device_count())
else:
    USE_CUDA = False


dataset = corefdataset(params.glove_file,params.data_file,params.embedding_size,params.batch_size,params.hiddens)

model = MEUNET(dataset, params.embedding_size,
                   params.hiddens,
                   params.nof_lstms,
                   params.dropout,
                   params.bidir)

if USE_CUDA:
    model.cuda()
model.load_state_dict(torch.load('best_model.pt')['net'].state_dict())


s=['The Jesus goes to Jerusalem every morning to see the great wall of Jerusalem .']

maxseqlensent=len(s[0].split())
nearestpowerof2=1
while nearestpowerof2<maxseqlensent:
    nearestpowerof2=nearestpowerof2*2
points=np.zeros((len(s),nearestpowerof2))
for i,c in enumerate(s):
    d1=c.split()
    for j,d in enumerate(d1):
        points[i,j]=dataset.vocab[d]

points=torch.from_numpy(points).cuda().long()

import pickle
out=model(points)
out=out.data.cpu().numpy()
out=np.argmax(out,axis=1)
print(out.shape)
for i in range(out.shape[2]):
	if int(out[0,0,i])==1:
		try:

			print(s[0].split()[i])
		except:
			pass
	else:
		print('------------')
np.savetxt('out.txt',out[0,:,:])
plt.imshow(out[0,:,:])
plt.show()